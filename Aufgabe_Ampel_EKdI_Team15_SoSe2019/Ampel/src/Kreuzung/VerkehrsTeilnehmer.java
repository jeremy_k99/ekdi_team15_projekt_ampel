package Kreuzung;

public abstract class VerkehrsTeilnehmer {

	public char zeichen;
	public int nummer;
	public int x;
	public int y;
	// oldX and oldY needed if the Verkehrsteilnehmer has to stop.
	public int oldX, oldY;
	// Save the last symbol the verkehrsteilnehmer steped on to put it back later
	// when it left the Patch.
	public char lastSymbol = ' ';

	// This variable define something like a search radius for the searching of
	// traffic lights. This is important because otherwise some traffic lights
	// couldn't be detected correctly.
	public int ampelLeftTolerance, ampelRightTolerance;

	public VerkehrsTeilnehmer(int nummer, int x, int y) {
		this.nummer = nummer;
		this.x = x;
		this.y = y;
	}

	public abstract boolean tick();
}
