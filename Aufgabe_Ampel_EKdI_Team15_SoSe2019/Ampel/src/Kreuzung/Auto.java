package Kreuzung;

import Kreuzung.enums.EAutoRichtung;

public class Auto extends VerkehrsTeilnehmer {

	EAutoRichtung richtung;
	public static int createdCars = 0;

	public Auto(int nummer, EAutoRichtung richtung) {
		super(nummer, richtung.getStartX(), richtung.getStartY());
		this.richtung = richtung;
		zeichen = 'A';
		createdCars++;
	}

	@Override
	public boolean tick() {
		oldX = x;
		oldY = y;
		if (lastSymbol != 'A' && lastSymbol != 'B' && lastSymbol != 'P') {
			Simulation.map.set(lastSymbol, x, y);
		}
		if (x == richtung.getEndX() && y == richtung.getEndY()) {
			return true;
		}
		switch (richtung) {
		case NS: {
			ampelLeftTolerance = 1;
			ampelRightTolerance = 0;
			y++;
			if (y > 8 && y <= 17) {
				x++;
			}
			break;
		}
		case SN: {
			ampelLeftTolerance = 0;
			ampelRightTolerance = 0;
			y--;
			if (y >= 8 && y < 17) {
				x--;
			}
			break;
		}
		case WO: {
			ampelLeftTolerance = 0;
			ampelRightTolerance = 0;
			x += 2;
			break;
		}
		case OW: {
			ampelLeftTolerance = 0;
			ampelRightTolerance = 1;
			x -= 2;
			break;
		}
		}
		Ampel ampel = Simulation.getAmpelAt(x, y, ampelRightTolerance, ampelLeftTolerance);
		if ((ampel != null && ampel instanceof AutoAmpel && ampel.getGruen() == false) || Simulation.map.getSymbol(x, y) == 'A') {
			x = oldX;
			y = oldY;
		} else {
			lastSymbol = Simulation.map.getSymbol(x, y);
		}
		Simulation.map.set(zeichen, x, y);
		return false;
	}
	
}