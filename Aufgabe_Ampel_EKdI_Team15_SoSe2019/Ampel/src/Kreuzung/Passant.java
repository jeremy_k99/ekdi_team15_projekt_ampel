package Kreuzung;

import Kreuzung.enums.EPassantRichtung;

public class Passant extends VerkehrsTeilnehmer {

	EPassantRichtung richtung;
	public static int createdPassanten = 0;

	public Passant(int nummer, EPassantRichtung richtung) {
		super(nummer, richtung.getStartX(), richtung.getStartY());
		this.richtung = richtung;
		zeichen = 'P';
		createdPassanten++;
	}

	@Override
	public boolean tick() {
		oldX = x;
		oldY = y;
		if (lastSymbol != 'A' && lastSymbol != 'B' && lastSymbol != 'P') {
			Simulation.map.set(lastSymbol, x, y);
		}
		if (x == richtung.getEndX() && y == richtung.getEndY()) {
			return true;
		}
		switch (richtung) {
		case RNS:
		case LNS: {
			ampelLeftTolerance = 1;
			ampelRightTolerance = 0;
			if (Simulation.time % 2 == 0) {
				y++;
			}

			break;
		}
		case LSN:
		case RSN: {
			ampelLeftTolerance = 0;
			ampelRightTolerance = 0;
			if (Simulation.time % 2 == 0) {
				y--;
			}
			break;
		}
		case OWO:
		case UWO: {
			ampelLeftTolerance = 0;
			ampelRightTolerance = 0;
			x++;
			break;
		}
		case UOW:
		case OOW: {
			ampelLeftTolerance = 0;
			ampelRightTolerance = 0;
			x--;
			break;
		}
		}
		Ampel ampel = Simulation.getAmpelAt(x, y, ampelRightTolerance, ampelLeftTolerance);
		if(ampel != null && ampel instanceof PassantAmpel && ampel.getGruen() == false) {
			x = oldX;
			y = oldY;
		} else {
			lastSymbol = Simulation.map.getSymbol(x, y);
		}
		Simulation.map.set(zeichen, x, y);
		return false;
	}

}