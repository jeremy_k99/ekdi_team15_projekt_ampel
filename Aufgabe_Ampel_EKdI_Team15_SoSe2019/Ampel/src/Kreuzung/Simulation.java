package Kreuzung;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Random;

import Kreuzung.enums.EAutoRichtung;
import Kreuzung.enums.EBahnRichtung;
import Kreuzung.enums.EKreuzungState;
import Kreuzung.enums.EPassantRichtung;

public class Simulation {

	public static final int P_PASSANT = 5, P_AUTO = 10, P_BAHN = 70;
	public static final int TICK_LENGTH = 1000;
	public static final int SIMULATION_LENGTH = 240;

	public static Rasterer map = new Rasterer();
	public static VerkehrsTeilnehmer[] teilnehmer = new VerkehrsTeilnehmer[240];
	public static Ampel[] ampeln = new Ampel[18];
	public static int time = 0;
	public static EKreuzungState kreuzungsStatus = EKreuzungState.HORIZONTAL_GREEN;
	public static Random generator = new Random();
	public static int bahnenInSimulation = 0;
	public static EKreuzungState lastState = EKreuzungState.HORIZONTAL_GREEN;
	
	public static PrintWriter writer = null;

	public static void main(String[] args) {
		long timestamp = System.currentTimeMillis();
		try {
			writer = new PrintWriter("src\\SimulationLogs\\simulation_" + timestamp + ".log", "UTF-8");
		} catch (FileNotFoundException | UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		writer.println("Console output of simulation " + timestamp + ":");
		run();
		writer.close();
	}

	public static void run() {
		map.initMap();
		initAmpeln();
		setAmpelStates();
		map.updateAmpeln();

		while (time < SIMULATION_LENGTH) {
			checkForCrash();
			tick();
			try {
				Thread.sleep(TICK_LENGTH);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	public static void checkForCrash() {
		for (int i = 0; i < teilnehmer.length; i++) {
			if (teilnehmer[i] == null) {
				continue;
			}
			for (int j = 0; j < teilnehmer.length; j++) {
				if (i != j) {
					if (teilnehmer[j] == null) {
						continue;
					}
					if (teilnehmer[j].x == teilnehmer[i].x && teilnehmer[i].y == teilnehmer[j].y) {
						if(!(teilnehmer[i] instanceof Passant && teilnehmer[j] instanceof Passant)) {
							int crashX = teilnehmer[i].x;
							int crashY = teilnehmer[i].y;
							map.set('X', crashX + 2, crashY);
							map.set('X', crashX - 2, crashY);
							map.set('X', crashX, crashY + 2);
							map.set('X', crashX, crashY - 2);
							map.show();
							writer.println("Crash at: x: " + crashX + ", y: " + crashY);
							writer.close();
							throw new IllegalStateException("Crash at: x: " + crashX + ", y: " + crashY);
						}
					}
				}
			}
		}
	}

	public static void tick() {
		setAmpelStates();
		if (generator.nextInt(P_PASSANT) == 0) {
			creatPassant();
		}
		if (generator.nextInt(P_AUTO) == 0) {
			creatAuto();
		}
		if (generator.nextInt(P_BAHN) == 0) {
			creatBahn();
			bahnenInSimulation++;
			kreuzungsStatus = EKreuzungState.TRAIN_COMING;
		}

		if (time % 30 == 0 && time != 0) {
			if (kreuzungsStatus == EKreuzungState.HORIZONTAL_GREEN) {
				kreuzungsStatus = EKreuzungState.TRANSITION;
			} else if (kreuzungsStatus == EKreuzungState.VERTICAL_GREEN) {
				kreuzungsStatus = EKreuzungState.TRANSITION;
			}
		}
		if (time % 30 == 12 && kreuzungsStatus != EKreuzungState.TRAIN_COMING) {
			if (kreuzungsStatus == EKreuzungState.TRANSITION && lastState == EKreuzungState.HORIZONTAL_GREEN) {
				kreuzungsStatus = EKreuzungState.VERTICAL_GREEN;
				lastState = EKreuzungState.VERTICAL_GREEN;
			} else if (kreuzungsStatus == EKreuzungState.TRANSITION && lastState == EKreuzungState.VERTICAL_GREEN) {
				kreuzungsStatus = EKreuzungState.HORIZONTAL_GREEN;
				lastState = EKreuzungState.HORIZONTAL_GREEN;
			}
		}

		map.updateAmpeln();
		for (VerkehrsTeilnehmer v : teilnehmer) {
			if (v != null) {
				boolean remove = v.tick();
				if (remove) {
					boolean removed = removeVerkehrsteilnehmer(v);
					if (removed == v instanceof Bahn) {
						bahnenInSimulation--;
						if (bahnenInSimulation == 0) {
							if (lastState == EKreuzungState.HORIZONTAL_GREEN) {
								kreuzungsStatus = EKreuzungState.VERTICAL_GREEN;
								lastState = EKreuzungState.VERTICAL_GREEN;
							} else {
								kreuzungsStatus = EKreuzungState.HORIZONTAL_GREEN;
								lastState = EKreuzungState.HORIZONTAL_GREEN;
							}
						}
					}
				}
			}
		}
		map.show();
		time++;

	}

	public static void initAmpeln() {
		Simulation.ampeln[0] = new PassantAmpel(15, 8, '0', true);
		Simulation.ampeln[1] = new PassantAmpel(18, 7, '0', true);
		Simulation.ampeln[2] = new PassantAmpel(27, 7, '2', true);
		Simulation.ampeln[3] = new PassantAmpel(24, 8, '2', true);
		Simulation.ampeln[4] = new PassantAmpel(10, 12, 'a', false);
		Simulation.ampeln[5] = new PassantAmpel(13, 13, 'a', false);
		Simulation.ampeln[6] = new PassantAmpel(29, 12, '4', false);
		Simulation.ampeln[7] = new PassantAmpel(32, 13, '4', false);
		Simulation.ampeln[8] = new PassantAmpel(18, 18, '8', true);
		Simulation.ampeln[9] = new PassantAmpel(15, 19, '8', true);
		Simulation.ampeln[10] = new PassantAmpel(27, 18, '6', true);
		Simulation.ampeln[11] = new PassantAmpel(24, 19, '6', true);
		Simulation.ampeln[12] = new AutoAmpel(15, 7, '1', false);
		Simulation.ampeln[13] = new AutoAmpel(27, 19, '1', false);
		Simulation.ampeln[14] = new AutoAmpel(10, 13, '5', true);
		Simulation.ampeln[15] = new AutoAmpel(32, 12, '5', true);
		Simulation.ampeln[16] = new BahnAmpel(24, 7, '3', false);
		Simulation.ampeln[17] = new BahnAmpel(18, 19, '3', false);

	}

	public static void setAmpelStates() {
		switch (kreuzungsStatus) {
		case HORIZONTAL_GREEN: {
			setAmpelNummer('0', true);
			setAmpelNummer('2', true);
			setAmpelNummer('5', true);
			setAmpelNummer('6', true);
			setAmpelNummer('8', true);
			setAmpelNummer('1', false);
			setAmpelNummer('3', false);
			setAmpelNummer('4', false);
			setAmpelNummer('a', false);
			break;
		}
		case VERTICAL_GREEN: {
			setAmpelNummer('0', false);
			setAmpelNummer('2', true);
			setAmpelNummer('5', false);
			setAmpelNummer('6', false);
			setAmpelNummer('8', true);
			setAmpelNummer('1', true);
			setAmpelNummer('3', false);
			setAmpelNummer('4', true);
			setAmpelNummer('a', true);
			break;
		}
		case TRAIN_COMING: {
			setAmpelNummer('0', false);
			setAmpelNummer('2', false);
			setAmpelNummer('5', false);
			setAmpelNummer('6', false);
			setAmpelNummer('8', false);
			setAmpelNummer('1', false);
			setAmpelNummer('3', true);
			setAmpelNummer('4', false);
			setAmpelNummer('a', false);
			break;
		}
		case TRANSITION: {
			setAmpelNummer('3', false);
			setAmpelNummer('1', false);
			setAmpelNummer('0', false);
			setAmpelNummer('6', false);
			setAmpelNummer('2', true);
			setAmpelNummer('8', true);
			setAmpelNummer('5', false);
			setAmpelNummer('4', false);
			setAmpelNummer('a', false);
		}
		default:
			break;
		}
	}

	public static void toggleAmpelNummer(char number) {
		for (int i = 0; i < ampeln.length; i++) {
			if (ampeln[i] != null && ampeln[i].getNumber() == number) {
				ampeln[i].toggle();
			}
		}
	}

	public static void setAmpelNummer(char number, boolean gruen) {
		for (int i = 0; i < ampeln.length; i++) {
			if (ampeln[i] != null && ampeln[i].getNumber() == number) {
				if (ampeln[i].getGruen() == gruen) {
					return;
				} else {
					toggleAmpelNummer(number);
					return;
				}
			}
		}
	}

	public static boolean removeVerkehrsteilnehmer(VerkehrsTeilnehmer t) {
		boolean found = false;
		for (int i = 0; i < teilnehmer.length && !found; i++) {
			if (teilnehmer[i] != null && teilnehmer[i].equals(t)) {
				teilnehmer[i] = null;
				found = true;
			}
		}
		return found;
	}

	public static boolean insertVerkehrsteilnehmer(VerkehrsTeilnehmer t) {
		for (int i = 0; i < teilnehmer.length; i++) {
			if (teilnehmer[i] == null) {
				for(int j = 0; j < teilnehmer.length; j++) {
					if(teilnehmer[j] == null) {
						continue;
					} else if(t.x == teilnehmer[j].oldX && t.y == teilnehmer[j].y) {
						return false;
					}
				}
				teilnehmer[i] = t;
				return true;
			}
		}
		return false;
	}

	public static Ampel getAmpelAt(int x, int y, int rightTolerance, int leftTolerance) {
		for (int i = 0; i < ampeln.length; i++) {
			if ((ampeln[i].getX() == x || ampeln[i].getX() == x + rightTolerance || ampeln[i].getX() == x - leftTolerance)
					&& ampeln[i].getY() == y) {
				return ampeln[i];
			}
		}
		return null;
	}

	public static void creatPassant() {
		insertVerkehrsteilnehmer(new Passant(Passant.createdPassanten, EPassantRichtung.passantRandom()));

	}

	public static void creatAuto() {
		insertVerkehrsteilnehmer(new Auto(Auto.createdCars, EAutoRichtung.autoRandom()));

	}

	public static void creatBahn() {
		insertVerkehrsteilnehmer(new Bahn(Bahn.createdTrains, EBahnRichtung.bahnRandom()));

	}

}