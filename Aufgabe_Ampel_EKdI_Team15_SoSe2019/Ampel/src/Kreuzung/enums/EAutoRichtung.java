package Kreuzung.enums;

import java.util.Random;

public enum EAutoRichtung {
	WO(4, 13, 44, 13), OW(44, 12, 4, 12), NS(16, 3, 25, 22), SN(27, 22, 18, 3);
	
	private int startX, startY, endX, endY;

	private EAutoRichtung(int startX, int startY, int endX, int endY) {
		this.startX = startX;
		this.startY = startY;
		this.endX = endX;
		this.endY = endY;
	}

	public int getStartX() {
		return startX;
	}

	public int getStartY() {
		return startY;
	}

	public int getEndX() {
		return endX;
	}

	public int getEndY() {
		return endY;
	}
	public static EAutoRichtung autoRandom(){
		EAutoRichtung[] auto = EAutoRichtung.values();
	    Random generator = new Random();
	    return auto[generator.nextInt(auto.length)];
	    }
}
