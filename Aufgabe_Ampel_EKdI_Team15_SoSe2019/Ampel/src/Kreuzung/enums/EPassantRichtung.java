package Kreuzung.enums;

import java.util.Random;

public enum EPassantRichtung {
	// RNS:Rechts Nord-S�d, LNS:Links Nord-S�d, OWO:Oben West-Ost, UWO:Unten
	// West-Ost
	RNS(30, 3, 30, 22), RSN(32, 22, 32, 3), LNS(11, 3, 11, 22), LSN(13, 22, 13, 3), OWO(7, 8, 44, 8), OOW(44, 7, 7, 7),
	UWO(7, 19, 44, 19), UOW(44, 18, 7, 18);

	private int startX, startY, endX, endY;

	private EPassantRichtung(int startX, int startY, int endX, int endY) {
		this.startX = startX;
		this.startY = startY;
		this.endX = endX;
		this.endY = endY;
	}

	public int getStartX() {
		return startX;
	}

	public int getStartY() {
		return startY;
	}

	public int getEndX() {
		return endX;
	}

	public int getEndY() {
		return endY;
	}
	public static EPassantRichtung passantRandom(){
		EPassantRichtung[] passanten = EPassantRichtung.values();
	    Random generator = new Random();
	    return passanten[generator.nextInt(passanten.length)];
	    }
}
