package Kreuzung.enums;

import java.util.Random;

public enum EBahnRichtung {
	BNS(25, 3, 16, 22), BSN(18, 22, 27, 3);
	
	private int startX, startY, endX, endY;

	private EBahnRichtung(int startX, int startY, int endX, int endY) {
		this.startX = startX;
		this.startY = startY;
		this.endX = endX;
		this.endY = endY;
	}

	public int getStartX() {
		return startX;
	}

	public int getStartY() {
		return startY;
	}

	public int getEndX() {
		return endX;
	}

	public int getEndY() {
		return endY;
	}
	public static EBahnRichtung bahnRandom(){
		EBahnRichtung[] bahn = EBahnRichtung.values();
	    Random generator = new Random();
	    return bahn[generator.nextInt(bahn.length)];
	    }



}
