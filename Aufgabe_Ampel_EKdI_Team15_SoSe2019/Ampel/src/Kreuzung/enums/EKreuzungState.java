package Kreuzung.enums;

public enum EKreuzungState {
	TRANSITION, HORIZONTAL_GREEN, VERTICAL_GREEN, TRAIN_COMING;
}