package Kreuzung;

public abstract class Ampel {

	protected boolean gruen;
	protected int x;
	protected int y;
	protected char number;

	public Ampel(int x, int y, char number, boolean gruen) {
		this.x = x;
		this.y = y;
		this.number = number;
		this.gruen = gruen;
	}
	
	public void toggle() {
		if (gruen) {
			gruen = false;
		} else {
			gruen = true;
		}
	}

	public boolean getGruen() {
		return gruen;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public char getNumber() {
		return number;
	}

}