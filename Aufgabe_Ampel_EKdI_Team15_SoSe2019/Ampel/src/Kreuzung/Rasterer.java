package Kreuzung;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
public class Rasterer {
	private final static int W = 52;
	private final static int H = 24;
	private char[][] screen;

	Rasterer() {
		screen = new char[H][W];
		for (int i = 0; i < H; ++i) {
			for (int k = 0; k < W; ++k) {
				screen[i][k] = ' ';
			}
		}
	}

	public void set(char c, int x, int y) {
		if (x < 0 || x >= W || y < 0 || y >= H)
			return;
		screen[y][x] = c;
	}

	public void show() {
		for (int i = 0; i < H; ++i) {
			System.out.println(new String(screen[i]));
			Simulation.writer.println(new String(screen[i]));
		}
	}

	public char getSymbol(int x, int y) {
		return screen[y][x];
	}

	public void initMap() {
		set('N', 1, 0);
		set('O', 2, 1);
		set('S', 1, 2);
		set('W', 0, 1);
		writeStringAt("Passant", 7, 2, true);
		writeStringAt("Auto", 16, 2, true);
		writeStringAt("Bahn(neu)", 21, 2, true);
		writeStringAt("Passant", 31, 2, true);
		writeStringAt("...................................................", 0, 7, true);
		writeStringAt("Passant", 0, 7, true);
		writeStringAt("....................", 12, 3, false);
		writeStringAt("....................", 31, 3, false);
		writeStringAt("_________________________________________________", 0, 12, true);
		writeStringAt("Auto", 0, 12, true);
		writeStringAt("...................................................", 0, 18, true);
		writeStringAt("Passant", 0, 18, true);
		writeStringAt("Passant", 45, 7, true);
		writeStringAt("Passant", 7, 23, true);
		writeStringAt("Passant", 45, 18, true);
		writeStringAt("Auto", 45, 12, true);
		writeStringAt("Bahn", 16, 23, true);
		writeStringAt("Auto", 25, 23, true);
		writeStringAt("Passant", 31, 23, true);
		writeStringAt("|||||", 17, 3, false);
		writeStringAt("|||||", 26, 3, false);
		writeStringAt("|||||", 17, 18, false);
		writeStringAt("|||||", 26, 18, false);
		for (int i = 17, j = 8; i < 27; i++, j++) {
			set('\\', i, j);
		}
		for (int i = 26, j = 8; j < 18; i--, j++) {
			set('/', i, j);
		}
		updateAmpeln();
	}

	public void updateAmpeln() {
		for (int i = 0; i < Simulation.ampeln.length; i++) {
			if (Simulation.ampeln[i] != null) {
				Ampel ampel = Simulation.ampeln[i];
				String ampelString = ampel.getGruen() ? "g" : "r";
				writeStringAt(ampelString + ampel.getNumber(), ampel.getX(), ampel.getY(), true);
			}
		}
	}

	public void writeStringAt(String stringToPrint, int x, int y, boolean horizontal) {
		char[] charString = stringToPrint.toCharArray();
		if (horizontal) {
			for (int i = x; i < x + charString.length; i++) {

				set(charString[i - x], i, y);
			}
		} else {
			for (int i = y; i < y + charString.length; i++) {
				set(charString[i - y], x, i);
			}
		}
	}
}