package Kreuzung;

import Kreuzung.enums.EBahnRichtung;

public class Bahn extends VerkehrsTeilnehmer {

	EBahnRichtung richtung;
	public static int createdTrains = 0;

	public Bahn(int nummer, EBahnRichtung richtung) {
		super(nummer, richtung.getStartX(), richtung.getStartY());
		this.richtung = richtung;
		zeichen = 'B';
		createdTrains++;
	}

	@Override
	public boolean tick() {
		oldX = x;
		oldY = y;
		if (lastSymbol != 'A' && lastSymbol != 'B' && lastSymbol != 'P') {
			Simulation.map.set(lastSymbol, x, y);
		}
		if (x == richtung.getEndX() && y == richtung.getEndY()) {
			return true;
		}
		switch (richtung) {
		case BNS: {
			ampelLeftTolerance = 1;
			ampelRightTolerance = 0;
			y++;
			if (y > 8 && y <= 17) {
				x--;
			}
			break;
		}
		case BSN: {
			ampelLeftTolerance = 0;
			ampelRightTolerance = 1;
			y--;
			if (y >= 8 && y < 17) {
				x++;
			}
			break;

		}
		}
		Ampel ampel = Simulation.getAmpelAt(x, y, ampelRightTolerance, ampelLeftTolerance);
		if (ampel != null && ampel instanceof BahnAmpel && ampel.getGruen() == false) {
			x = oldX;
			y = oldY;
		} else {
			lastSymbol = Simulation.map.getSymbol(x, y);
		}
		Simulation.map.set(zeichen, x, y);
		return false;
	}
	
}