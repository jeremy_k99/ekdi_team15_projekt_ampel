\babel@toc {ngerman}{}
\contentsline {section}{\numberline {1}Pr\IeC {\"a}sentation von Arbeitsergebnissen}{4}% 
\contentsline {subsection}{\numberline {1.1}Beschreibung der Pr\IeC {\"a}sentation}{4}% 
\contentsline {subsection}{\numberline {1.2}Visualisierung}{6}% 
\contentsline {subsection}{\numberline {1.3}Erarbeitung des Papers}{7}% 
\contentsline {section}{\numberline {2}Lerntheorien und Zeitmanagement}{8}% 
\contentsline {subsection}{\numberline {2.1}Einzelleistung von Henry Behrend}{8}% 
\contentsline {subsubsection}{\numberline {2.1.1}Wie habe ich bisher gelernt?}{9}% 
\contentsline {subsubsection}{\numberline {2.1.2}Was habe aus den Tutorien und Vorlesungen des Moduls mitgenommen und wie lerne ich jetzt?}{10}% 
\contentsline {subsubsection}{\numberline {2.1.3}Begr\IeC {\"u}ndung des eigenen Lernverhaltens}{11}% 
\contentsline {subsection}{\numberline {2.2}Einzelleistung von Ching-Chia Lin}{13}% 
\contentsline {subsubsection}{\numberline {2.2.1}Wie lerne ich?}{13}% 
\contentsline {subsection}{\numberline {2.3}Einzelleistung von Jeremy Kaefert}{16}% 
\contentsline {subsubsection}{\numberline {2.3.1}Wie habe ich bisher gelernt?}{16}% 
\contentsline {subsubsection}{\numberline {2.3.2}Wie lerne ich jetzt, durch neue Erkenntnisse \IeC {\"u}ber das Lernplakat, dem Auseinandersetzen mit einem individuellen Lernplan und der Analyse der Zeiterfassung?}{17}% 
\contentsline {section}{\numberline {3}Reflektion der Gruppenarbeit}{20}% 
\contentsline {subsection}{\numberline {3.1}Erstellen der Protokolle}{20}% 
\contentsline {subsection}{\numberline {3.2}Reflektion der Arbeitsweise}{21}% 
\contentsline {section}{\numberline {4}Literaturverzeichnis}{22}% 
\contentsline {section}{\numberline {5}Anhang}{23}% 
