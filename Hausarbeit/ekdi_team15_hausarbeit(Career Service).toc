\babel@toc {ngerman}{}
\contentsline {section}{\numberline {1}Einleitung}{4}
\contentsline {section}{\numberline {2}Projektmanagement}{4}
\contentsline {subsection}{\numberline {2.1}Erstellung des SOLL-Projektplanes}{4}
\contentsline {subsection}{\numberline {2.2}Vergleich: SOLL-Plan und IST-Zustand}{6}
\contentsline {section}{\numberline {3}Teamrollen}{7}
\contentsline {subsection}{\numberline {3.1}Teamrollen - Henry Behrend}{7}
\contentsline {subsubsection}{\numberline {3.1.1}Pers\IeC {\"o}nliche Teamrolle}{7}
\contentsline {subsubsection}{\numberline {3.1.2}Individueller Beitrag zum Projekt}{8}
\contentsline {subsubsection}{\numberline {3.1.3}Bedeutung des Teamrollenmodells}{9}
\contentsline {subsection}{\numberline {3.2}Teamrollen - Johannes Peter H\IeC {\"o}cker}{9}
\contentsline {subsubsection}{\numberline {3.2.1}Pers\IeC {\"o}nliche Teamrolle}{9}
\contentsline {subsubsection}{\numberline {3.2.2}Individueller Beitrag zum Projekt}{10}
\contentsline {subsubsection}{\numberline {3.2.3}Bedeutung des Teamrollenmodells}{10}
\contentsline {subsection}{\numberline {3.3}Teamrollen - Jeremy K\IeC {\"a}fert}{10}
\contentsline {subsubsection}{\numberline {3.3.1}Pers\IeC {\"o}nliche Teamrolle}{10}
\contentsline {subsubsection}{\numberline {3.3.2}Individueller Beitrag zum Projekt}{12}
\contentsline {subsubsection}{\numberline {3.3.3}Bedeutung des Teamrollenmodells}{13}
\contentsline {subsection}{\numberline {3.4}Teamrollen - Ching-Chia Lin}{14}
\contentsline {subsubsection}{\numberline {3.4.1}Pers\IeC {\"o}nliche Teamrolle}{14}
\contentsline {subsubsection}{\numberline {3.4.2}Individueller Beitrag zum Projekt}{14}
\contentsline {subsubsection}{\numberline {3.4.3}Bedeutung des Teamrollenmodells}{14}
\contentsline {section}{\numberline {4}Teamphasen}{14}
\contentsline {subsection}{\numberline {4.1}Theoretische Teamphasen bei der Projektarbeit}{14}
\contentsline {subsection}{\numberline {4.2}Potential und Schwierigkeiten der Teamphasen}{17}
\contentsline {section}{\numberline {5}Motivation}{19}
\contentsline {subsection}{\numberline {5.1}Definition der Team-Motivation}{19}
\contentsline {subsection}{\numberline {5.2}Beispiele f\IeC {\"u}r die Team-Motivation}{20}
\contentsline {section}{\numberline {6}Fazit}{21}
\contentsline {subsection}{\numberline {6.1}Fazit - Henry Behrend}{21}
\contentsline {subsubsection}{\numberline {6.1.1}Was hat sich bew\IeC {\"a}hrt?}{21}
\contentsline {subsubsection}{\numberline {6.1.2}\IeC {\"A}nderungspotential f\IeC {\"u}r zuk\IeC {\"u}nftige Teamarbeit}{22}
\contentsline {subsection}{\numberline {6.2}Fazit - Johannes Peter H\IeC {\"o}cker}{23}
\contentsline {subsubsection}{\numberline {6.2.1}Was hat sich bew\IeC {\"a}hrt?}{23}
\contentsline {subsubsection}{\numberline {6.2.2}\IeC {\"A}nderungspotential f\IeC {\"u}r zuk\IeC {\"u}nftige Teamarbeit}{23}
\contentsline {subsection}{\numberline {6.3}Fazit - Jeremy K\IeC {\"a}fert}{24}
\contentsline {subsubsection}{\numberline {6.3.1}Was hat sich bew\IeC {\"a}hrt?}{24}
\contentsline {subsubsection}{\numberline {6.3.2}\IeC {\"A}nderungspotential f\IeC {\"u}r zuk\IeC {\"u}nftige Teamarbeit}{24}
\contentsline {subsection}{\numberline {6.4}Fazit - Ching-Chia Lin}{25}
\contentsline {subsubsection}{\numberline {6.4.1}Was hat sich bew\IeC {\"a}hrt?}{25}
\contentsline {subsubsection}{\numberline {6.4.2}\IeC {\"A}nderungspotential f\IeC {\"u}r zuk\IeC {\"u}nftige Teamarbeit}{25}
